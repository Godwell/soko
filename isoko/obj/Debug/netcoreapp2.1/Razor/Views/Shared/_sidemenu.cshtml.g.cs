#pragma checksum "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "97aef1d5fb99d8cd0850019f5c93dd62cb24316f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__sidemenu), @"mvc.1.0.view", @"/Views/Shared/_sidemenu.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/_sidemenu.cshtml", typeof(AspNetCore.Views_Shared__sidemenu))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Code\Personal Projects\isoko\isoko\Views\_ViewImports.cshtml"
using isoko;

#line default
#line hidden
#line 2 "D:\Code\Personal Projects\isoko\isoko\Views\_ViewImports.cshtml"
using isoko.Models;

#line default
#line hidden
#line 1 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"97aef1d5fb99d8cd0850019f5c93dd62cb24316f", @"/Views/Shared/_sidemenu.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ec87a840656b9dd5360b073e1ea544171e126572", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__sidemenu : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(123, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
#line 6 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
  
    string Title = com.GetTitle(User.Identity.Name);

#line default
#line hidden
            BeginContext(188, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 10 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
 if (Title == "Admin")
{

#line default
#line hidden
            BeginContext(217, 8, true);
            WriteLiteral("    <li>");
            EndContext();
            BeginContext(226, 44, false);
#line 12 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
   Write(Html.ActionLink("Users", "Index", "Account"));

#line default
#line hidden
            EndContext();
            BeginContext(270, 15, true);
            WriteLiteral("</li>\r\n    <li>");
            EndContext();
            BeginContext(286, 46, false);
#line 13 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
   Write(Html.ActionLink("Reports", "Index", "Reports"));

#line default
#line hidden
            EndContext();
            BeginContext(332, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 14 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
}

#line default
#line hidden
#line 15 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
 if (Title == "Merchant")
{

#line default
#line hidden
            BeginContext(372, 8, true);
            WriteLiteral("    <li>");
            EndContext();
            BeginContext(381, 44, false);
#line 17 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
   Write(Html.ActionLink("Users", "Index", "Account"));

#line default
#line hidden
            EndContext();
            BeginContext(425, 15, true);
            WriteLiteral("</li>\r\n    <li>");
            EndContext();
            BeginContext(441, 46, false);
#line 18 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
   Write(Html.ActionLink("Reports", "Index", "Reports"));

#line default
#line hidden
            EndContext();
            BeginContext(487, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 19 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
}

#line default
#line hidden
#line 20 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
 if (Title == "PSP")
{

#line default
#line hidden
            BeginContext(522, 8, true);
            WriteLiteral("    <li>");
            EndContext();
            BeginContext(531, 44, false);
#line 22 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
   Write(Html.ActionLink("Users", "Index", "Account"));

#line default
#line hidden
            EndContext();
            BeginContext(575, 15, true);
            WriteLiteral("</li>\r\n    <li>");
            EndContext();
            BeginContext(591, 51, false);
#line 23 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
   Write(Html.ActionLink("Make Payment", "Index", "Payment"));

#line default
#line hidden
            EndContext();
            BeginContext(642, 15, true);
            WriteLiteral("</li>\r\n    <li>");
            EndContext();
            BeginContext(658, 46, false);
#line 24 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
   Write(Html.ActionLink("Reports", "Index", "Reports"));

#line default
#line hidden
            EndContext();
            BeginContext(704, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 25 "D:\Code\Personal Projects\isoko\isoko\Views\Shared\_sidemenu.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public isoko.Common.Commons com { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<ApplicationUser> usermanager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
