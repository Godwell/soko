﻿using FluentValidation;
using isoko.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace isoko.Validations
{
    public class AccountValidator : AbstractValidator<AspNetUsers>
    {
        public AccountValidator()
        {
            RuleFor(x => x.UserName).NotNull().WithMessage("Username Is Required");
            RuleFor(x => x.Email).NotNull().WithMessage("Email Address Is Required")
                .EmailAddress().WithMessage("A Valid Email Address Is Required");
            RuleFor(x => x.Names).NotNull().WithMessage("Names are Required");
            RuleFor(x => x.PhoneNumber).NotNull().WithMessage("PhoneNumber is Required");
            RuleFor(x => x.Address).NotNull().WithMessage("Address is Required");
            RuleFor(x => x.Country).NotNull().WithMessage("Country is Required");
            RuleFor(x => x.IdNumber).NotNull().WithMessage("IdNumber is Required");
            RuleFor(x => x.UserType).NotNull().WithMessage("UserType is Required");
        }
    }
}
