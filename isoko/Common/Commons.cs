﻿using isoko.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace isoko.Common
{
    public class Commons
    {
        private IsokopayContext _db;

        public Commons(IsokopayContext db)
        {
            _db = db;
        }
        public string GetTitle(string username)
        {
            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == username);
            string Type = usr.UserType;
            return Type;
        }
        public static string GetPath()
        {
            string path = "C:\\isokologs";
                //Server.MapPath("/Content/AppLogs");

            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }
            return path;
        }
        public static void LogMessageToFile(string message, string s_filetype)
        {
            //yyyy,mm,dd
            string s_filename = "unknowntypes.txt";

            switch (s_filetype.ToLower())
            {
                case "error":
                    s_filename = "Err" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".txt";
                    break;
                case "info":
                    s_filename = "Info" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".txt";
                    break;
                case "mailerror":
                    s_filename = "MailErr" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".txt";
                    break;
                case "test":
                    s_filename = "test" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".txt";
                    break;
            }

            try
            {
                StreamWriter sw = File.AppendText(GetPath() + s_filename);
                string logLine = String.Format("{0:G}: {1}.", DateTime.Now, message);
                sw.WriteLine(logLine);
                sw.Close();
            }
            catch
            {
                //nothing to record here
            }
        }
    }
}
