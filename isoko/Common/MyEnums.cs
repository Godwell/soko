﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace isoko.Common
{
    public class MyEnums
    {
        public enum StatusOption
        {
            Paid = 1,
            Rejected = 2,
        }
        public enum ShopOrderStatus
        {
            New = 1,
            Processing = 2,
            Hold = 3,
            Canceled = 4,
            Done = 5,
            Failed = 6,
        }
    }
}
