﻿using System;
using System.Collections.Generic;

namespace isoko.Models
{
    public partial class UserType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
