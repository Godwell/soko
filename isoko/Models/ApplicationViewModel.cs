﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace isoko.Models
{
    public class ApplicationViewModel
    {
        public class TokenViewModel
        {
            public string UserName { get; set; }
            public string PasswordHarsh { get; set; }
        }
        public class MerchantPaymentViewModel
        {
            public string MerchantName { get; set; }
            public int OrderId { get; set; }
            public string UserEmail { get; set; }
            public string Product { get; set; }
            public int UnitPrice { get; set; }
            public int Quantity { get; set; }
            public int Tax { get; set; }
            public int IsokopayFee { get; set; }
            public int Shipping { get; set; }
            public int TotalPrice { get; set; }
        }
        public class LoginViewModel
        {
            [Key]
            public int id { get; set; }
            [Required]
            public string UserName { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [Display(Name = "Remember Me")]
            public bool RememberMe { get; set; }

        }
        public class ResetPasswordViewModel
        {
            [Required]
            [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$|^\+?\d{0,2}\-?\d{4,5}\-?\d{5,6}", ErrorMessage = "Please enter a valid email address")]
            [Display(Name = "User Name")]
            public string UserName { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Old Password")]
            public string OldPassword { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "New Password")]
            public string Password { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            [Display(Name = "Confirm Password")]
            public string ConfirmPassword { get; set; }
        }
        public class PaymentViewModel
        {
            public int? orderid { get; set; }
            public int paymentstatus { get; set; }
            public int shippingstatus { get; set; }
        }
        public class MerchantViewModel
        {
            public string MerchantName { get; set; }
            public int? OrderId { get; set; }
            public string Email { get; set; }
            public string Product { get; set; }
            public double? TotalPrice { get; set; }
        }
    }
}
