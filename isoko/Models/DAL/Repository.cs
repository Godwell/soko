﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace isoko.Models.DAL
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private IsokopayContext _context;
        private DbSet<T> _dbEntity;

        public Repository()
        {
            _context = new IsokopayContext();
            _dbEntity = _context.Set<T>();
        }
        public void DeleteModel(long modelId)
        {
            T model = _dbEntity.Find(modelId);
            _dbEntity.Remove(model);
        }

        public IEnumerable<T> GetModel()
        {
            return _dbEntity.ToList();
        }

        public T GetModelByID(long modelId)
        {
            return _dbEntity.Find(modelId);
        }

        public void InsertModel(T model)
        {
            _dbEntity.Add(model);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void UpdateModel(T model)
        {
            _context.Entry(model).State = EntityState.Modified;
        } 
    }
}
