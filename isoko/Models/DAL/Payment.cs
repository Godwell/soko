﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace isoko.Models.DAL
{
    public class Payment : IPayment
    {
        private readonly IsokopayContext context;

        public Payment(IsokopayContext context)
        {
            this.context = context;
        }
        public MerchantPayment Add(MerchantPayment merchantPayment)
        {
            context.MerchantPayment.Add(merchantPayment);
            context.SaveChanges();
            return merchantPayment;
        }
    }
}
