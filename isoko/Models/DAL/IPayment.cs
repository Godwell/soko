﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace isoko.Models.DAL
{
    public interface IPayment
    {
        MerchantPayment Add(MerchantPayment merchantPayment);
    }
}
