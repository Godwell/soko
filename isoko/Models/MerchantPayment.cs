﻿using System;
using System.Collections.Generic;

namespace isoko.Models
{
    public partial class MerchantPayment
    {
        public long Id { get; set; }
        public string MerchantName { get; set; }
        public int? OrderId { get; set; }
        public string UserEmail { get; set; }
        public string Product { get; set; }
        public double? UnitPrice { get; set; }
        public double? TotalPrice { get; set; }
        public int? Quantity { get; set; }
        public double? Tax { get; set; }
        public double? IsokopayFee { get; set; }
        public double? Shipping { get; set; }
        public int? Status { get; set; }
    }
}
