﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace isoko.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Names { get; set; }
        public string IdNumber { get; set; }
        public string UniqueNo { get; set; }
        public string UserType { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public bool FirstTimeLogin { get; set; }
    }
}
