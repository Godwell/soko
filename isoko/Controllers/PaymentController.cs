﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using isoko.Common;
using isoko.Models;
using isoko.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using X.PagedList;
using static isoko.Models.ApplicationViewModel;

namespace isoko.Controllers
{
    public class PaymentController : Controller
    {
        private IsokopayContext _db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private IRepository<MerchantPayment> interfaceObj;
        public PaymentController(IsokopayContext db, IHttpContextAccessor httpContextAccessor)
        {
            _db = db;
            _httpContextAccessor = httpContextAccessor;
            this.interfaceObj = new Repository<MerchantPayment>();
        }
        public IActionResult Index(string currentFilter, string Status, string search, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var payment = _db.MerchantPayment.ToList();

            if(Status == "UserEmail")
            {
                payment = payment.Where(s => s.UserEmail.StartsWith(search)).ToList();
                return View(payment.ToPagedList(pageNumber, pageSize));
            }
            if(Status == "OrderId")
            {
                int orderId = Convert.ToInt32(search);
                payment = payment.Where(s => s.OrderId == orderId).ToList();
                return View(payment.ToPagedList(pageNumber, pageSize));
            }
            return View(payment.ToPagedList(pageNumber, pageSize));
        }
        [HttpGet]
        public IActionResult Payment(long Id)
        {
            
            var Payment = _db.MerchantPayment.Find(Id);
            double? Total = Payment.TotalPrice + Payment.IsokopayFee;
            Payment.TotalPrice = Total;

            MerchantViewModel model = new MerchantViewModel();
            model.MerchantName = Payment.MerchantName;
            model.OrderId = Payment.OrderId;
            model.Email = Payment.UserEmail;
            model.Product = Payment.Product;
            model.TotalPrice = Total;
            return View(model);
        }
        [HttpPost]
        public IActionResult Payment(MerchantViewModel model)
        {
            var mp = _db.MerchantPayment.OrderByDescending(x => x.Id).FirstOrDefault(x => x.OrderId == model.OrderId);
            mp.Status = (int)MyEnums.ShopOrderStatus.Done;
            interfaceObj.UpdateModel(mp);
            interfaceObj.Save();
            PaymentViewModel vm = new PaymentViewModel();
            vm.orderid = mp.OrderId;
            vm.paymentstatus =(int) MyEnums.ShopOrderStatus.Done;
            vm.shippingstatus =(int) MyEnums.ShopOrderStatus.Done;
            string myJson = JsonConvert.SerializeObject(vm);
            string resp = Post(myJson);

            if (resp.Length > 0)
            {
                TempData["successmessage"] = "Saved successfully";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                TempData["successmessage"] = "Encountered An Error";
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
        public string Post(string myJson)
        {
            string response = "";
            string url = "http://198.38.85.25:1234/s-cart/public/api/status";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = myJson.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            {
                requestWriter.Write(myJson);
            }

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    response = responseReader.ReadToEnd();  
                }
            }
            catch (Exception e)
            {
                
            }
            return response;
        }

    }
}