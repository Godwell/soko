﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using isoko.Common;
using isoko.Models;
using isoko.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static isoko.Models.ApplicationViewModel;

namespace isoko.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantPaymentController : ControllerBase
    {
        
        [HttpPost]
        public IActionResult CreatePayment([FromBody] MerchantPaymentViewModel model)
        {
            IsokopayContext isokopayContext = new IsokopayContext();
            try
            {
                int isokopayFee = 10;
                double shipping = model.Shipping;
                double price = model.UnitPrice * model.Quantity;
                double isokopayF = price * isokopayFee / 100;
                double Total = price + shipping;
                MerchantPayment merchantPayment = new MerchantPayment();
                merchantPayment.MerchantName = model.MerchantName;
                merchantPayment.OrderId = model.OrderId;
                merchantPayment.UserEmail = model.UserEmail;
                merchantPayment.Product = model.Product;
                merchantPayment.UnitPrice = model.UnitPrice;
                merchantPayment.Quantity = model.Quantity;
                merchantPayment.IsokopayFee = isokopayF;
                merchantPayment.TotalPrice = Total;
                merchantPayment.Shipping = shipping;
                merchantPayment.Status = (int)MyEnums.ShopOrderStatus.New;
                isokopayContext.Add(merchantPayment);
                isokopayContext.SaveChanges();

                return Ok(merchantPayment);
            }
            catch(Exception ex)
            {
                //ex.StackTrace.ToString
                //Request req = new Request();
                //req.Request1 = ex.ToString();
                //isokopayContext.Add(req);
                //isokopayContext.SaveChanges();
                ex.Data.Clear();
                return Ok(ex.ToString());
            }
            
        }
    }
}