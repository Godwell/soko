﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using isoko.Common;
using isoko.Data;
using isoko.Models;
using isoko.Models.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using static isoko.Models.ApplicationViewModel;

namespace isoko.Controllers
{
    //[Authorize]
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private IRepository<AspNetUsers> interfaceObj;
        private IsokopayContext _db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEmailSender _emailSender;
        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IsokopayContext db, ApplicationDbContext applicationDbContext, IHttpContextAccessor httpContextAccessor, IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            this.interfaceObj = new Repository<AspNetUsers>();
            _db = db;
            _applicationDbContext = applicationDbContext;
            _httpContextAccessor = httpContextAccessor;
            _emailSender = emailSender;
        }
        // GET: Account
        public ActionResult Index(string currentFilter, string Status, string search, int? page)
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var ussr = _db.AspNetUsers.OrderByDescending(x => x.Id).FirstOrDefault(x => x.UserName == userId);
            if(ussr.UserType == "Admin")
            {
                var acc = _db.AspNetUsers.ToList();
                return View(acc);
            }
            if (ussr.UserType == "Merchant")
            {
                var acc = _db.AspNetUsers.Where(s => s.UserType == "Merchant").ToList();
                return View(acc);
            }
            if (ussr.UserType == "PSP")
            {
                var acc = _db.AspNetUsers.Where(s => s.UserType == "PSP").ToList();
                return View(acc);
            }
            return View();
        }

        // GET: Account/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        [AllowAnonymous]
        // GET: Account/Create
        public ActionResult Create()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usrs = _db.AspNetUsers.OrderByDescending(x => x.Id).FirstOrDefault(x => x.UserName == userId);
            List<UserType> ussr = _db.UserType.ToList();
            if(usrs.UserType == "Admin")
            {
                ViewBag.Utype = ussr;
            }
            if (usrs.UserType == "Merchant")
            {
                ussr = ussr.Where(x => x.Name == "Merchant").ToList();
                ViewBag.Utype = ussr;
            }
            if (usrs.UserType == "PSP")
            {
                ussr = ussr.Where(x => x.Name == "PSP").ToList();
                ViewBag.Utype = ussr;
            }
            return View();
        }
        [AllowAnonymous]

        // POST: Account/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AspNetUsers aspNetUsers)
        {
            try
            {
                string Unique = aspNetUsers.Email + "_1234";
                var usrs = _db.AspNetUsers.Where(x => x.Email == aspNetUsers.Email).ToList();
                if (usrs.Count == 0)
                {
                    if (ModelState.IsValid)
                    {
                        string pass = "Wellcome254@";
                        var user = new ApplicationUser
                        {
                            UserName = aspNetUsers.UserName,
                            Email = aspNetUsers.Email,
                            PhoneNumber = aspNetUsers.PhoneNumber,
                            Address = aspNetUsers.Address,
                            Country = aspNetUsers.Country,
                            IdNumber = aspNetUsers.IdNumber,
                            Names = aspNetUsers.Names,
                            UserType = aspNetUsers.UserType,
                            FirstTimeLogin = true,
                            UniqueNo = Unique
                        };
                        var result = await _userManager.CreateAsync(user, pass);

                        if (result.Succeeded)
                        {
                            string password = "Your Password is " + pass;
                            await TestAction(password);
                        }
                        

                    }
                }
                    // TODO: Add insert logic here
                    return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }

        }
        public async Task TestAction(string password)
        {
            await _emailSender.SendEmailAsync("godwellm@yahoo.com", "Password", password);
        }
        // GET: Account/Edit/5
        public ActionResult Edit(string Id)
        {
            try
            {
                var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
                var usrs = _db.AspNetUsers.OrderByDescending(x => x.Id).FirstOrDefault(x => x.UserName == userId);
                List<UserType> ussr = _db.UserType.ToList();
                if (usrs.UserType == "Admin")
                {
                    ViewBag.Utype = ussr;
                }
                if (usrs.UserType == "Merchant")
                {
                    ussr = ussr.Where(x => x.Name == "Merchant").ToList();
                    ViewBag.Utype = ussr;
                }
                if (usrs.UserType == "PSP")
                {
                    ussr = ussr.Where(x => x.Name == "PSP").ToList();
                    ViewBag.Utype = ussr;
                }

                AspNetUsers aspNetUsers = _db.AspNetUsers.Find(Id);
                return View(aspNetUsers);
            }
            catch(Exception ex)
            {
                ex.Data.Clear();
            }

            return View();
        }

        // POST: Account/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AspNetUsers aspNetUsers)
        {
            try
            {
                interfaceObj.UpdateModel(aspNetUsers);
                interfaceObj.Save();
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Delete/5
        public ActionResult Delete(string Id)
        {
            try
            {
                AspNetUsers users = _db.AspNetUsers.Find(Id);
                _db.AspNetUsers.Remove(users);
                _db.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                ex.Data.Clear();
            }
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(loginViewModel.UserName, loginViewModel.Password, loginViewModel.RememberMe, false);

                if (result.Succeeded)
                {
                    var usr = _db.AspNetUsers.FirstOrDefault(x => x.UserName == loginViewModel.UserName);
                    if(usr.FirstTimeLogin == false)
                    {
                        Commons c = new Commons(_db);
                        c.GetTitle(loginViewModel.UserName);
                        String Message = loginViewModel.UserName + " Has Successful Logged In";
                        //Common.Commons.LogMessageToFile(Message, "Info");
                        return RedirectToAction("About", "Home");
                    }
                    return RedirectToAction("ResetPassword");
                    
                }
            }
            catch(Exception ex)
            {
                ex.Data.Clear();
            }
            
            return View();
        }
        public IActionResult ResetPassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ResetPassword(ResetPasswordViewModel resetPasswordViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(resetPasswordViewModel);
            }
            return View();
        }
        public IActionResult Logout()
        {
            return RedirectToAction("Login");
        }
    }
}