﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using isoko.Data;
using isoko.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using static isoko.Models.ApplicationViewModel;

namespace isoko.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController1 : ControllerBase
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public AccountController1(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext applicationDbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _applicationDbContext = applicationDbContext;
        }
        [HttpPost]
        public async Task <IActionResult> GenerateToken([FromBody] TokenViewModel model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.UserName, model.PasswordHarsh, false, false);

            if (result.Succeeded)
            {
                return Ok();
            }

            return Ok();
        
        }
    }
}