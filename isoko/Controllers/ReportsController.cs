﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using isoko.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace isoko.Controllers
{
    public class ReportsController : Controller
    {
        private IsokopayContext _db;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ReportsController(IsokopayContext db, IHttpContextAccessor httpContextAccessor)
        {
            _db = db;
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index(string currentFilter, string Status, string search, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usrs = _db.AspNetUsers.OrderByDescending(x => x.Id).FirstOrDefault(x => x.UserName == userId);

            if(Status == null)
            {
                if(usrs.UserType == "Admin")
                {
                    var ussr = _db.MerchantPayment.ToList();
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
                if (usrs.UserType == "Merchant")
                {
                    var ussr = _db.MerchantPayment.Where(x => x.UserEmail == usrs.Email);
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
                if (usrs.UserType == "PSP")
                {
                    var ussr = _db.MerchantPayment.ToList();
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
            }
            if (Status == "5")
            {
                if (usrs.UserType == "Admin")
                {
                    var ussr = _db.MerchantPayment.Where(x=>x.Status == 5).ToList();
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
                if (usrs.UserType == "Merchant")
                {
                    var ussr = _db.MerchantPayment.Where(x => x.UserEmail == usrs.Email && x.Status == 5);
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
                if (usrs.UserType == "PSP")
                {
                    var ussr = _db.MerchantPayment.Where(x => x.Status == 5).ToList();
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
            }
            else
            {
                if (usrs.UserType == "Admin")
                {
                    var ussr = _db.MerchantPayment.ToList();
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
                if (usrs.UserType == "Merchant")
                {
                    var ussr = _db.MerchantPayment.Where(x => x.UserEmail == usrs.Email);
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
                if (usrs.UserType == "PSP")
                {
                    var ussr = _db.MerchantPayment.ToList();
                    return View(ussr.ToPagedList(pageNumber, pageSize));
                }
            }
            
            return View();
        }
    }
}